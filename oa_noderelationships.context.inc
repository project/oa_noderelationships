<?php

/**
 * Implementation of hook_context_default_contexts().
 */
function oa_noderelationships_context_default_contexts() {
  $export = array();
  $context = new stdClass;
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'oa_noderelationships';
  $context->description = '';
  $context->tag = 'oa_noderelationships';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'noderelationships/*' => 'noderelationships/*',
        'views/ajax/noderef' => 'views/ajax/noderef',
      ),
    ),
    'views' => array(
      'values' => array(
        'noderelationships_noderef' => 'noderelationships_noderef',
        'noderelationships_noderef:page_grid' => 'noderelationships_noderef:page_grid',
        'noderelationships_noderef:page_table' => 'noderelationships_noderef:page_table',
      ),
    ),
  );
  $context->reactions = array(
    'context_reaction_active_theme' => array(
      'theme' => 'garland',
    ),
    'oa_noderelationships_views_ajax_url' => array(
      'url' => 'noderef',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('oa_noderelationships');

  $export['oa_noderelationships'] = $context;
  return $export;
}
