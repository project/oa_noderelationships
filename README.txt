Open Atrium's Ginkgo theme doesn't play nicely with the Node Relationships module. This module provides a Context definition that forces the Garland theme on Node Relationships' forms, so in particular, selecting a row works on "Search and Reference".

Also required for this module: http://drupal.org/project/context_reaction_theme

== INSTALLATION == 

To install, enable both the context_reaction_theme module and the oa_noderelationships module, then go to /admin/build/themes and click on the 'enable' checkbox next to the Garland theme. Next time you click on a Node Relationships button and the modal box opens you'll notice the Garland theme is enabled in that box and clicking on rows for selection works.


== CREDIT ==

rjstatic of Gravitek Labs, http://graviteklabs.com
